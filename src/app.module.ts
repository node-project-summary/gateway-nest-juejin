/*
 * @Author: ningyongheng ningyongheng@jeejio.com
 * @Date: 2024-03-13 14:57:37
 * @LastEditors: ningyongheng ningyongheng@jeejio.com
 * @LastEditTime: 2024-03-13 17:03:40
 * @FilePath: /gateway-nest-juejin/src/app.module.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { getConfig } from './utils';
// 增加全局的多环境配置模块,根目录创建.env 文件变量定义如下所示，配置后会默认读取此文件:
// DATABASE_USER=test
// yaml自定义全局配置方法：load 方法中传入的 getConfig 是一个函数，并不是直接 JSON 格式的配置对象，直接添加变量会报错
@Module({
  imports: [
    // 这里应该注意到，我们并没有注册 ConfigModule。
    // 这是因为在 app.module 中添加 isGlobal 属性，开启 Config 全局注册，如果 isGlobal 没有添加的话，则需要先在对应的 module 文件中注册后才能正常使用 ConfigService。
    ConfigModule.forRoot({
      ignoreEnvFile: true,
      isGlobal: true,
      load: [getConfig],
    }),
    UserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
