declare const module: any;

import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { VERSION_NEUTRAL, VersioningType } from '@nestjs/common';

// 全局异常报错过滤器
import { AllExceptionsFilter } from './common/exceptions/base.exception.filter';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './common/exceptions/http.exception.filter';
/*
 * @Author: ningyongheng ningyongheng@jeejio.com
 * @Date: 2024-03-13 14:57:37
 * @LastEditors: ningyongheng ningyongheng@jeejio.com
 * @LastEditTime: 2024-03-13 16:27:14
 * @FilePath: /gateway-nest-juejin/src/main.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE'
 */
import { NestFactory } from '@nestjs/core';
import { TransformInterceptor } from './common/interceptors/transform.interceptor';
import { generateDocument } from './doc';

async function bootstrap() {
  //const app = await NestFactory.create(AppModule);
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  );

  // 接口版本化管理
  app.enableVersioning({
    // 新增全局版本管理配置
    // defaultVersion: '1',
    // 兼容多个版本
    defaultVersion: [VERSION_NEUTRAL, '1', '2'],
    type: VersioningType.URI,
  });
  // 接口响应拦截器
  app.useGlobalInterceptors(new TransformInterceptor());
  // 异常过滤器
  app.useGlobalFilters(new AllExceptionsFilter(), new HttpExceptionFilter());
  // 创建文档
  generateDocument(app);

  // 热更新的功能看自己的需求再开启，有的时候存在缓存的情况出现，
  // 另外，在使用热更新的时候，数据库章节中实体类需要手动注册，不能自动注册，
  // 所以如果项目不大的啥情况，使用 NestJS 自带的项目启动脚本即可。
  // Webpack并不会自动将（例如 graphql 文件）复制到 dist 文件夹中。同理，Webpack 与静态路径（例如 TypeOrmModule 中的 entities 属性）不兼容。
  // 所以如果有同学跳过本章，直接配置了 TypeOrmModule 中的 entities，反过来再直接配置热重载会导致启动失败
  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
  // 监听端口必须写在启动main的最下面，不然会导致端口重复启动多个
  await app.listen(3001);
}
bootstrap();
