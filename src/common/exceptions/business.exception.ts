/*
 * @Author: ningyongheng ningyongheng@jeejio.com
 * @Date: 2024-03-13 16:30:13
 * @LastEditors: ningyongheng ningyongheng@jeejio.com
 * @LastEditTime: 2024-03-13 16:33:14
 * @FilePath: /gateway-nest-juejin/dist/business.exception.ts
 * @Description: 处理全局异常拦截外，主动的业务运行未知异常：business.exception.ts 来处理业务运行中预知且主动抛出的异常：
 * 那么需要：简单改造一下 HttpExceptionFilter，在处理 HTTP 异常返回之前先处理业务异常：


 */
import { HttpException, HttpStatus } from '@nestjs/common';

import { BUSINESS_ERROR_CODE } from './business.error.codes';

type BusinessError = {
  code: number;
  message: string;
};

export class BusinessException extends HttpException {
  constructor(err: BusinessError | string) {
    if (typeof err === 'string') {
      err = {
        code: BUSINESS_ERROR_CODE.COMMON,
        message: err,
      };
    }
    super(err, HttpStatus.OK);
  }

  static throwForbidden() {
    throw new BusinessException({
      code: BUSINESS_ERROR_CODE.ACCESS_FORBIDDEN,
      message: '抱歉哦，您无此权限！',
    });
  }
}
