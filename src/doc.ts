/*
 * @Author: ningyongheng ningyongheng@jeejio.com
 * @Date: 2024-03-13 17:13:51
 * @LastEditors: ningyongheng ningyongheng@jeejio.com
 * @LastEditTime: 2024-03-13 17:15:25
 * @FilePath: /gateway-nest-juejin/src/doc.ts
 * @Description: 为了节约配置项，Swagger 的配置信息全部取自 package.json，有额外需求的话可以自己维护配置信息的文件。


 */
import * as packageConfig from '../package.json';

import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export const generateDocument = (app) => {
  const options = new DocumentBuilder()
    .setTitle(packageConfig.name)
    .setDescription(packageConfig.description)
    .setVersion(packageConfig.version)
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('/api/doc', app, document);
};
